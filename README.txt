Welcome to Node author block module

REQUIREMENTS
------------

 * No special requirements.

INSTALLATION
------------

 * Module: Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/7/extending-drupal-7/overview
   for further information.

CONFIGURATION
-------------

 * The module has no menu or modifiable settings. There is no configuration.

HOW TO USE
----------
 * Go to admin/config/media/image-styles/add. Create new image style name 'Node author thumbnail'. Select effect 'Scale' and set 50 Width, 50 Height, Allow Upscaling.

 * Go to admin/people/permissions. Set the access permission for this module.

 * Go to admin/structure/block. Configure 'Node author information' block, set the content type you want to show author block.

 * Uncheck the default 'Display author and date information' for your content type.

MAINTAINERS
-----------

Current maintainers:
 * Thanh Pham - https://www.drupal.org/u/tphamcs


